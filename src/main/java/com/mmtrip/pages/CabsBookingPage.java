package com.mmtrip.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class CabsBookingPage {

    public WebDriver driver;
    public WebDriverWait w;

    private final By fromCity = By.id("fromCity");
    private final By from = By.xpath("//input[@placeholder='From']");
    private final By iata = By.cssSelector("span[class*='blackText']");
    private final By listSearches = By.cssSelector("ul[class='react-autosuggest__suggestions-list']");
    private final By airportName = By.cssSelector("p[class*='appendBottom3']");
    private final By to = By.xpath("//input[@placeholder='To']");
    private final By currentMonth = By.cssSelector("div.DayPicker-Body");
    private final By daysOfMonths = By.cssSelector("div.DayPicker-Day");
    private final By nextMonth=By.xpath("//span[@aria-label='Next Month']");
    private final By labelMonth=By.xpath("//div[@role='heading']//div");
    private final By timeDropDown=By.cssSelector("ul.timeDropDown.blackText");
    private final By hour= By.tagName("li");
    private final By returnDate = By.xpath("//p[@class='latoBlack greyText']");
    private final By search=By.cssSelector("a[class*='widgetSearchBtn']");

    public WebElement getReturnDate() {
        return driver.findElement(returnDate);
    }

    public WebElement getNextMonth() {
        return driver.findElement(nextMonth);
    }

    public WebElement getAirportName() {
        return driver.findElement(airportName);
    }

    public CabsBookingPage(WebDriver driver, WebDriverWait w) {

        this.driver = driver;
        this.w = w;

    }

    public WebElement getFromCity() {
        return driver.findElement(fromCity);
    }

    public WebElement getFrom() {
        return driver.findElement(from);
    }

    public List<WebElement> getSuggestions() {
        WebElement lista = driver.findElement(listSearches);

        try {
            w.until(ExpectedConditions.numberOfElementsToBeLessThan(airportName, 20));
        } catch (org.openqa.selenium.TimeoutException e) {
            System.out.println("search terminated");
        }

        return lista.findElements(iata);

    }

    public void selectAirport(List<WebElement> suggestions, String airport) {
        for (WebElement suggestion : suggestions) {
            if (suggestion.getText().contains(airport)) {
                suggestion.click();
                break;
            }
        }
    }

    public WebElement getTo() {
        return driver.findElement(to);
    }

    public WebElement getDate() {

        if(!getLabelMonth().getText().contains("July"))
        {
            System.out.println("No estamos en Julio");
            getNextMonth().click();
        }

        WebElement Calendar = driver.findElement(currentMonth);
        List<WebElement> days = Calendar.findElements(daysOfMonths);
        WebElement selectedDay = days.get(0);
        for (WebElement day : days) {
            if (day.getText().equals("9")) {
                System.out.println(day.getText());
                selectedDay = day;
            }
        }
        return selectedDay;
    }

    public WebElement getLabelMonth() {
        return driver.findElement(labelMonth);
    }

    public WebElement getHour()
    {
        WebElement pickUpTime=driver.findElement(timeDropDown);
        List<WebElement> hours=pickUpTime.findElements(hour);
        WebElement selectedHour=hours.get(0);
        for(WebElement hour:hours)
        {
            if(hour.getText().contains("06:30"))
            {
                selectedHour=hour;
                break;
            }
        }
        return selectedHour;
    }

    public WebElement getSearch() {
        return driver.findElement(search);
    }
}
