package com.mmtrip.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;


public class GirftcardPage {
	
public WebDriver driver;
	
	public GirftcardPage(WebDriver driver) {
		// TODO Auto-generated constructor stub
		this.driver=driver;
		}
	
	By avoidPop = By.xpath("//*[@id=\'SW\']/div[1]/div[1]/ul/li[5]/div[2]");
	
	By countryOpts = By.xpath("//*[@id=\'SW\']/div[1]/div[1]/ul/li[6]/div[1]/p[2]/span[3]");
	
	By country = By.xpath("//*[@id=\'SW\']/div[1]/div[1]/ul/li[6]/div[2]/p[1]/span[2]");
	
	By moreOpts = By.xpath("//*[@id=\'SW\']/div[1]/div[2]/div/div/nav/ul/li[10]");
	
	By giftCards = By.xpath("//a[@data-cy='submenu_Giftcards']");
	
	By bdCard = By.xpath("//img[@src='https://promos.makemytrip.com/appfest/2x/580x346-birthday-1-phy-12012021.png']");
	
	By cardTheme = By.xpath("//li//img[@src='https://promos.makemytrip.com/appfest/2x/580x346-Birthday-2.png']");
	
	By amountInput = By.xpath("//*[@id=\'root\']/div/div[2]/div/div[1]/div/div[1]/div[1]/div/div[2]/div[3]/div/div[2]/div/p/input");
	
	
	By senderName = By.xpath("//input[@name='senderName']");
	
	By senderMobile =	By.xpath("//input[@name='senderMobileNo']");
	
	By senderMail = By.xpath("//input[@name='senderEmailId']");
	
	By buyBtn = By.xpath("//*[@id=\'root\']/div/div[2]/div/div[1]/div/div[2]/div[1]/div[1]/button");
	
	By payOpts = By.xpath("//*[@id=\'root\']/div/div/main/div[1]/div[3]/div[1]");
	
	public WebElement getPop() {
		
		return driver.findElement(avoidPop);
	
				
	}
	
	public WebElement getCOpts() {
		
		return driver.findElement(countryOpts);
	
				
	}

	public WebElement getCountry() {
	return driver.findElement(country);
	}
	
	
	public WebElement getGift() {
		return driver.findElement(giftCards);
	}
	
	public WebElement getACard() {
		return driver.findElement(bdCard);
	}
	public WebElement getTheme() {
		return driver.findElement(cardTheme);
		
		}
	
	public WebElement getAmount() {
		return driver.findElement(amountInput);
		
	}
	
	public WebElement getSender() {
		return driver.findElement(senderName);
		
	}
	
	public WebElement getMobile() {
		return driver.findElement(senderMobile);
		
	}
	public WebElement getEmail() {
		return driver.findElement(senderMail);
		
	}
	
	public WebElement setBuy() {
		return driver.findElement(buyBtn);
		
	}
	
	public String getPayOpts() {
		
		return driver.findElement(payOpts).getText();
		
		
	
				
	}

}
