package com.mmtrip.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class LandingPage {
	public WebDriver driver;
	MakeMyTripPage mmt=new MakeMyTripPage(driver);
	
	
	
	public LandingPage(WebDriver driver) {
		// TODO Auto-generated constructor stub
		this.driver=driver;
		}
		
		
		By avoidPop = By.xpath("//*[@id=\'SW\']/div[1]/div[1]/ul/li[5]/div[2]");
		
		By countryOpts = By.xpath("//*[@id=\'SW\']/div[1]/div[1]/ul/li[4]/div[1]/span[2]");
		
		By country = By.xpath("//*[@id=\'SW\']/div[1]/div[1]/ul/li[4]/div[2]/div[1]/div[1]/p");
		
		By usa = By.xpath("//*[@id=\'SW\']/div[1]/div[1]/ul/li[4]/div[2]/div[2]/div[2]/p[3]");
		
		By applyCountry = By.xpath("//*[@id=\'SW\']/div[1]/div[1]/ul/li[4]/div[2]/div[1]/button");
		
		public WebElement getPop() {
			
			return driver.findElement(avoidPop);
		
					
		}
		
		public WebElement getCountry() {
			
				return driver.findElement(countryOpts);
			
						
		}
		
		public WebElement getCountryName() {
			return driver.findElement(country);
		}
		
		public WebElement getUsa() {
			return driver.findElement(usa);
		}
	
		public WebElement applyCountry() {
			return driver.findElement(applyCountry);
		}
	
	/*	public void avoidPop() {
			 try {
					if(mmt.getAutoPop().isDisplayed())
					{
						mmt.getFlexible().click();
						System.out.println("Aparecio el popup");
					}
				} catch(Exception e)
				{
					System.out.println("No aparecio el Popup");
				}
			}*/
		
	
	//pass the id to a variable then turn it to an object.
	
	
	
	
	
	
	
	

}

