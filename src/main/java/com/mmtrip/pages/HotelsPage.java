package com.mmtrip.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class HotelsPage {
	public WebDriver driver;
	public WebDriverWait w;
	
	public HotelsPage(WebDriver driver) {
		this.driver=driver;
		}

	public HotelsPage(WebDriver driver, WebDriverWait w) {
		this.driver = driver;
		this.w = w;
	}

	By avoidPop = By.xpath("//*[@id=\'SW\']/div[1]/div[1]/ul/li[5]/div[2]");
		
		
		
		By countryOpts = By.xpath("//*[@id=\'SW\']/div[1]/div[1]/ul/li[6]/div[1]/p[2]/span[3]");
		
		By country = By.xpath("//*[@id=\'SW\']/div[1]/div[1]/ul/li[6]/div[2]/p[1]/span[2]");
		
		By hotels = By.xpath("//*[@id=\'SW\']/div[1]/div[2]/div/div/nav/ul/li[2]");
		
		
			
		
		By inputOrigin = By.xpath("//*[@id=\'root\']/div/div[2]/div/div/div[2]/div/div/div[1]");
		
		
	
		
		//By destination = By.xpath("//span[contains(text(),'To')]");
		
		By inputDestiny = By.cssSelector("input[placeholder='Enter city/ Hotel/ Area/ Building']");
		
		By phuket = By.xpath("//p[contains(text(),'Phuket, Thailand')]");
		
		
		
		By currentDate = By.cssSelector("div.DayPicker-Day--today");

		By returnDate = By.xpath("//*[@id=\'root\']/div/div[2]/div/div/div[2]/div/div/div[2]/div[1]/div/div/div/div[2]/div/div[2]/div[2]/div[3]/div[5]/div[3]");
		
		
		
		
		
		By roomGuests = By.xpath("//*[@id=\'guest\']");
		
		By adults = By.xpath("//li[@data-cy='adults-3']");
		
		By children = By.xpath("//li[@data-cy='children-1']");
		

		By buttonApply = By.cssSelector("button.btnApply");
		
		By travelReason = By.xpath("//*[@id=\'root\']/div/div[2]/div/div/div[2]/div/div/div[5]/label/span");
		
		By leisure = By.xpath("//li[@data-cy='travelFor-Leisure']");
		By searchBtn = By.cssSelector("button#hsw_search_button");
		
		By childAge = By.xpath("//*[@id=\'0\']");



		//private final By enterHotel=By.xpath("//input[contains(@class,'react-autosuggest__input react-autosuggest__input--open')]");

	private final By selectHtlCity=By.cssSelector("div[class*='selectHtlCity']");
	private final By enterHotel=By.xpath("//input[@placeholder='Enter city/ Hotel/ Area/ Building']");
	private final By listSearches = By.cssSelector("ul[class='react-autosuggest__suggestions-list']");
	private final By cityName = By.cssSelector("p[class*='appendBottom5']");
	private final By hotel=By.xpath("//*[contains(@class,'autoSuggestHtlResultType')]");
	private final By currentMonth = By.cssSelector("div.DayPicker-Body");
	private final By daysOfMonths = By.cssSelector("div.DayPicker-Day");
	private final By roomGuest = By.xpath("//div[@class='hsw_inputBox roomGuests  ']");
	private final By adultsHB = By.xpath("//li[@data-cy='adults-4']");
	private final By childrenHB = By.xpath("//li[@data-cy='children-2']");
	private final By apply = By.xpath("//button[@data-cy='submitGuest']");
	private final By ages=By.xpath("//*[contains(@data-cy,'childAgeValue-')]");
	private final By travelForReasonTxt=By.cssSelector("p[data-cy='travelForReasonTxt']");

	public WebElement getTravelForReasonTxt() {
		return driver.findElement(travelForReasonTxt);
	}

	public WebElement getEnterHotel() {
		return driver.findElement(enterHotel);
	}

	public WebElement getSelectHtlCity() {
		return driver.findElement(selectHtlCity);
	}

	public void waitEnterHotel(){
		w.until(ExpectedConditions.presenceOfElementLocated(getByEnterHotel()));
	}

	public By getByEnterHotel()
	{
		return enterHotel;
	}


	public WebElement getPop() {
			
			return driver.findElement(avoidPop);
		
					
		}

		public WebElement getCOpts() {
			
			return driver.findElement(countryOpts);
		
					
		}
	
		public WebElement getCountry() {
		return driver.findElement(country);
		}
		
		public WebElement getHotels() {
			return driver.findElement(hotels);
			}
		
		
		
	
		public WebElement getOrigin() {
	
			return driver.findElement(inputOrigin);

			
		}
		
		public WebElement getOpt() {
			
			return driver.findElement(phuket);

			
		}
		
		
		
		
		public WebElement getInputD() {
			
			return driver.findElement(inputDestiny);

			
		}
		
		public WebElement getDate() {
			
			return driver.findElement(currentDate);

			
		}
		public WebElement getReturn() {
			
			return driver.findElement(returnDate);

			
		}
		
        public WebElement getRoomGuests() {
			
			return driver.findElement(roomGuests);

			
		}
		public WebElement getAdults() {
			
			return driver.findElement(adults);

			
		}
		public WebElement getChildren() {
			
			return driver.findElement(children);

			
		}
		

		
		
		public WebElement applyFeatures() {
			
			return driver.findElement(buttonApply);

			
		}
		

		public WebElement getReason() {
			
			return driver.findElement(travelReason);

			
		}
		

		public WebElement getLeisure() {
			
			return driver.findElement(leisure);

			
		}
		
		
		public WebElement searchHotels() {
			
			return driver.findElement(searchBtn);

			
		}
	
	//pass the id to a variable then turn it to an object.
	
	
		public void getAge(String pick) {
			
			WebElement staticDropdown = driver.findElement(childAge);
			 Select dropdown =new Select(staticDropdown);
				
				dropdown.selectByVisibleText(pick);
			
			
		
					
		}
		
		public void closedriver(){
			 
			driver.close();
		 
		}

	public List<WebElement> getSuggestions() {
		WebElement lista = driver.findElement(listSearches);

		try {
			w.until(ExpectedConditions.numberOfElementsToBeLessThan(cityName, 20));
		} catch (org.openqa.selenium.TimeoutException e) {
			System.out.println("search terminated");
		}

		return lista.findElements(hotel);

	}

	public void selectHotel(List<WebElement> suggestions, String type) {
		for (WebElement suggestion : suggestions) {
			if (suggestion.getText().equals(type)) {
				suggestion.click();
				break;
			}
		}
	}

	public WebElement getCheckIn() {

		WebElement Calendar = driver.findElement(currentMonth);
		List<WebElement> days = Calendar.findElements(daysOfMonths);
		WebElement today = days.get(0);
		for (WebElement day : days) {
			if (day.getAttribute("class").contains("today")) {
				System.out.println(day.getAttribute("class"));
				today = day;
			}
		}
		return today;
	}

	public WebElement getCheckOut() {
		WebElement Calendar = driver.findElement(currentMonth);
		List<WebElement> days = Calendar.findElements(daysOfMonths);
		System.out.println(days.size());
		WebElement checkOutDay = days.get(0);
		int counter=1;
		for (int i=0;i<days.size();i++) {
			if (days.get(i).getAttribute("class").contains("today")) {
				for(int j=1;j<8;j++)
				{
					if(counter==7)
					{
						checkOutDay = days.get(i+j);
					}
					counter++;
				}

			}
		}
		return checkOutDay;

	}

	public WebElement getRoomGuest() {
		return driver.findElement(roomGuest);
	}

	public WebElement getAdultsHB() {
		return driver.findElement(adultsHB);
	}

	public WebElement getChildrenHB() {
		return driver.findElement(childrenHB);
	}

	public WebElement getApply() {
		return driver.findElement(apply);
	}

	public void selectChildAge(int child,String age) {
		getSelectChild(child).click();
		getSelectChildAge(child,age).click();
		getSelectChild(child).sendKeys(Keys.ENTER);
	}

	public WebElement getSelectChild(int child) {
		int index=child-1;
		By select=By.xpath("//select[@id='"+index+"']");
		return driver.findElement(select);
	}

	public WebElement getSelectChildAge(int child,String age) {
		WebElement scope= getSelectChild(child);
		List<WebElement> childAges;

		if(child==1)
		{
			childAges=scope.findElements(ages).subList(1,13);
		}
		else
			{
				childAges=scope.findElements(ages).subList(14,26);
			}

		WebElement selectedChildAge=childAges.get(0);
		for (WebElement childAge:childAges
			 ) {
			if(childAge.getText().contains(age))
			{
				selectedChildAge=childAge;
				break;
			}

		}
		return selectedChildAge;
	}

	public WebElement getSearchBtn() {
		return driver.findElement(searchBtn);
	}

	public void selectReason(String travelFor) {
		WebElement travelForPopUp=driver.findElement(By.cssSelector("ul.travelForPopup"));
		List<WebElement> reasons=travelForPopUp.findElements(By.tagName("li"));
		for (WebElement reason:reasons
			 ) {
			if(reason.getText().contains(travelFor))
			{
				reason.click();
				break;
			}

		}
	}
}
