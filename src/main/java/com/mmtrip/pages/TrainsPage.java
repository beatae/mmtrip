package com.mmtrip.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class TrainsPage {
	
public WebDriver driver;
	
	public TrainsPage(WebDriver driver) {
		// TODO Auto-generated constructor stub
		this.driver=driver;
		}
	
	By avoidPop = By.xpath("//*[@id=\'SW\']/div[1]/div[1]/ul/li[5]/div[2]");
	
	By countryOpts = By.xpath("//*[@id=\'SW\']/div[1]/div[1]/ul/li[6]/div[1]/p[2]/span[3]");
	
	By country = By.xpath("//*[@id=\'SW\']/div[1]/div[1]/ul/li[6]/div[2]/p[1]/span[2]");
	
	By trains = By.xpath("//*[@id=\'SW\']/div[1]/div[2]/div/div/nav/ul/li[5]/a");
	
	By liveTrain = By.xpath("//*[@id=\'root\']/div/div[2]/div/div/div/div[1]/span[3]");
	
	By selectTrain = By.xpath("//*[@id=\'root\']/div/div[2]/div/div/div/div[2]/div/div[1]/label/span");
	
	By inputOrigin = By.xpath("//*[@id=\'root\']/div/div[2]/div/div/div/div[2]/div/div[1]/div[1]/div/div/div/input");
	
	
	By pickOrigin = By.xpath("//*[@id='react-autowhatever-1']/div[1]/ul/li/div/p/span[contains(text(),'12724')]");
	

	
	By trainOption = By.xpath("//li/span[contains(text(),'NGP')]");
	
	By trainDate = By.xpath("//*[@id=\"root\"]/div/div[2]/div/div/div/div[2]/div/div[3]/ul/li[2]/span[1]");

	
	By checkStatus = By.xpath("//a[@data-cy='checkStatusButton']");
	
	By trainHour = By.cssSelector("div.trainStatusRight");
	
	
	
   
	
	
	
	public WebElement getPop() {
		
		return driver.findElement(avoidPop);
	
				
	}
	
	public WebElement getCOpts() {
		
		return driver.findElement(countryOpts);
	
				
	}

	public WebElement getCountry() {
	return driver.findElement(country);
	}
	
	
	public WebElement getTrains() {
		return driver.findElement(trains);
	}
	
	public WebElement getLiveStat() {
		return driver.findElement(liveTrain);
	}
	public WebElement getTrain() {
		
		return driver.findElement(selectTrain);

		
	}
	
	public WebElement getOrigin() {
		
		return driver.findElement(inputOrigin);

		
	}
	
	public WebElement setOrigin() {
		
		return driver.findElement(pickOrigin);

		
	}
	
	
	public WebElement setTStop() {
		
		return driver.findElement(trainOption);

		
	}
	
	public WebElement setDate() {
		
		return driver.findElement(trainDate);

		
	}
	
	public WebElement checkStatus() {
		
		return driver.findElement(checkStatus);

		
	}
	
	public String getTrainHour() {
		
		return driver.findElement(trainHour).getText();
		
		
	
				
	}
	
	
	

}
