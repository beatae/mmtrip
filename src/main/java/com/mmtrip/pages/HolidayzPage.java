package com.mmtrip.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class HolidayzPage {
    public WebDriver driver;
    public WebDriverWait w;

    private final By skipBtn=By.cssSelector("button.shipBtn");
    private final By nextButton=By.cssSelector("a.primaryBtn.fill");

    public WebElement getNextButton() {
        return driver.findElement(nextButton);
    }
    //<a href="javascript:void(0);" class="primaryBtn fill">Next</a>

    public WebElement getSkipBtn() {
        return driver.findElement(skipBtn);
    }
    public By getBySkipBtn()
    {
        return skipBtn;
    }

    public HolidayzPage(WebDriver driver, WebDriverWait w) {
        this.driver = driver;
        this.w = w;
    }

    public void evadeHelpButtons()
    {
        for (int i = 0; i <3 ; i++) {

            try {
                w.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("a.primaryBtn.fill")));
                getNextButton().click();
            } catch (org.openqa.selenium.NoSuchElementException e)
            {
                System.out.println("No aparecio el buton de llenar la forma");
            }catch (org.openqa.selenium.TimeoutException e)
            {
                System.out.println("No hay presencia del buton de llenar la forma");
            }
        }

    }

    public void evadeNextButton()
    {
        try {
            w.until(ExpectedConditions.elementToBeClickable(getSkipBtn()));
            getNextButton().click();
        } catch (org.openqa.selenium.NoSuchElementException e)
        {
            System.out.println("No aparecio el buton de llenar la forma");
        }
    }

    public void evadeSkipButton()
    {
        try{
            w.until(ExpectedConditions.presenceOfElementLocated(getBySkipBtn()));
            getSkipBtn().click();
        } catch(org.openqa.selenium.NoSuchElementException e )
        {
            System.out.println("No aparecio el  Popup");
        } catch(org.openqa.selenium.TimeoutException e)
        {
            System.out.println("Tiempo excedido");
        }
    }




}
