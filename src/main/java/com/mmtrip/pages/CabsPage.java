package com.mmtrip.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class CabsPage {

    public WebDriver driver;
    public WebDriverWait w;

    private final By cab=By.xpath("//div[@class='cabListingTileWrapper blackText']");

    public CabsPage(WebDriver driver,WebDriverWait w)
    {
        this.driver=driver;
        this.w=w;
    }

    public void getCabs()
    {
        w.until(ExpectedConditions.elementToBeClickable(cab));
        List<WebElement> cabs =driver.findElements(cab);
        System.out.println("There are: " + cabs.size()+" availables");
    }


}
