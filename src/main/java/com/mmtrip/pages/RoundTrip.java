package com.mmtrip.pages;



import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;


public class RoundTrip {
	public WebDriver driver;
	
	public RoundTrip(WebDriver driver) {
		// TODO Auto-generated constructor stub
		this.driver=driver;
		}
		
	
		By avoidPop = By.xpath("//*[@id=\'SW\']/div[1]");
		
		By roundTrip = By.xpath("//*[@id=\'root\']/div/div[2]/div/div/div[1]/ul/li[2]/span");
			
		
		By inputOrigin = By.xpath("//span[contains(text(),'From')]");
		
		
		By pickOrigin = By.xpath("//*[@id=\'react-autowhatever-1\']//ul/li/div/div/p[contains(text(),'New York, US')]");
		
		
		By destination = By.xpath("//span[contains(text(),'To')]");
		
		By inputDestiny = By.xpath("//input[@placeholder='To']");
		
		By bgk = By.xpath("//*[@id='react-autowhatever-1']//ul//li//div[contains(text(),'BKK')]");
		
		By currentDate = By.cssSelector("div.DayPicker-Day--today");

		By returnDate = By.xpath("//*[@id=\'root\']/div/div[2]/div/div/div[2]/div[1]/div[3]/div[1]/div/div/div/div[2]/div/div[2]/div[1]/div[3]/div[4]/div[3]");
		
		By travellerClass = By.xpath("//*[@id=\'root\']/div/div[2]/div/div/div[2]/div[1]/div[5]/label/span");
		
		By adults = By.xpath("//li[@data-cy='adults-3']");
		
		By children = By.xpath("//li[@data-cy='children-2']");
		
		By travelClass = By.xpath("//li[@data-cy='travelClass-3']");
		
		By buttonApply = By.cssSelector("button.btnApply");
		
		By searchBtn = By.xpath("//*[@id=\'root\']/div/div[2]/div/div/div[2]/p/a");
		
		public WebElement getPop() {
			
			return driver.findElement(avoidPop);
		
					
		}
		public WebElement getRt() {
			
			return driver.findElement(roundTrip);
		
					
		}
	
		public WebElement getOrigin() {
	
			return driver.findElement(inputOrigin);

			
		}
		
		public WebElement getOpt() {
			
			return driver.findElement(pickOrigin);

			
		}
		
		
		public WebElement getDestiny() {
			
			return driver.findElement(destination);

			
		}
		
		public WebElement getInputD() {
			
			return driver.findElement(inputDestiny);

			
		}
		public WebElement getDCity() {
			
			return driver.findElement(bgk);

			
		}
		public WebElement getDate() {
			
			return driver.findElement(currentDate);

			
		}
		public WebElement getReturn() {
			
			return driver.findElement(returnDate);

			
		}
		
        public WebElement getTravClass() {
			
			return driver.findElement(travellerClass);

			
		}
		public WebElement getAdults() {
			
			return driver.findElement(adults);

			
		}
		public WebElement getChildren() {
			
			return driver.findElement(children);

			
		}

		public WebElement getTClass() {
			
			return driver.findElement(travelClass);

			
		}
		
		public WebElement applyFeatures() {
			
			return driver.findElement(buttonApply);

			
		}
		
		
		public WebElement searchFlights() {
			
			return driver.findElement(searchBtn);

			
		}
		
		
		
	//pass the id to a variable then turn it to an object.
	
	
	
	
	
	
	
	

}
