package com.mmtrip.resources;

import java.io.File;
import java.io.IOException;
import java.util.Date;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.io.FileHandler;

public class ScreenshotCapture {
	
	
	public static WebDriver driver;


	public ScreenshotCapture(WebDriver driver) {
		// TODO Auto-generated constructor stub
	}


	public static void captureScreenshot() throws IOException {
		 
		Date d = new Date();
 
		String FileName = d.toString().replace(":", "_").replace(" ", "_") + ".png";
 
		File screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
 
		FileHandler.copy(screenshot, new File("/home/users/bhanu.pratap/Documents/testScreenshot/" + FileName));
 
	}
	
	
	

}
