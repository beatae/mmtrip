package com.mmtrip.resources;

import java.io.File;
import java.io.FileInputStream;

import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Base {
	
	public WebDriver driver;
	public Properties props;
	private final String path=System.getProperty("user.dir");
	private final String pathChrome = path + "\\drivers\\chromedriver.exe";
	private final String pathMozilla = path + "\\drivers\\geckodriver.exe";
	private final String pathProperties = path + "\\src\\main\\java\\com\\mmtrip\\resources\\data.properties";

	public WebDriver initializeDriver() throws IOException {
		
		System.out.println(path);
		Properties props = new Properties();
		FileInputStream fis = new FileInputStream(pathProperties);
		props.load(fis);
		String browserName = props.getProperty("browser");
		System.out.println(browserName);
		//opens makemytrip page
	
		
		
		
		
	
		
		if (browserName==null) {
			System.out.println("Default configuration");
			System.setProperty("webdriver.chrome.driver", pathChrome);
			driver = new ChromeDriver();
		} else {
		
		

		
		if(browserName.equals("chrome")) {
			System.setProperty("webdriver.chrome.driver",pathChrome );
			driver=new ChromeDriver();
			
		}
		else if(browserName.equals("firefox")){
			System.setProperty("webdriver.chrome.driver",pathMozilla );
			
			driver=new FirefoxDriver();
			
		}
		}
		
		
		String url = props.getProperty("URL");
		driver.get(url);
		System.out.println(url);
		driver.manage().window().maximize();
		
		fis.close();
		return driver;
		
		
		
		
	}
	//explicit wait
	
	public WebDriverWait initializeWait()
	{
		WebDriverWait w=new WebDriverWait(driver,10);
		return w;
	}
	
	
	//Getting screeshots of the errors for extentreports
	/*public void getScreenShotPath(String testCaseName, WebDriver driver) throws IOException {
		TakesScreenshot ts = (TakesScreenshot) driver;
		File source = ts.getScreenshotAs(OutputType.FILE);
		String destinFile = System.getProperty("user.dir" + "\\extentreports\\"+ testCaseName + ".png");
		FileUtils.copyFile(source,new File(destinFile));
	}*/

	
}
