package com.mmtrip.testcases;


import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.io.FileHandler;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.mmtrip.pages.HotelsPage;
import com.mmtrip.pages.MakeMyTripPage;
import com.mmtrip.resources.Base;
import com.mmtrip.resources.ScreenshotCapture;



public class Hotels_TestCase extends Base {
	
	public WebDriver driver;
	String picker = "2";
	 String city = "phuket";
	
	@BeforeTest
	public void startDriver() throws IOException{
		
		driver = initializeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		
	}
	
	@Test
	public void baseNavigation() throws IOException, InterruptedException {
				
		
		

		
		
		
		 
		 MakeMyTripPage mmt=new MakeMyTripPage(driver);
		 HotelsPage ht = new HotelsPage(driver);
		
		 
		 mmt.evadePopup();
		 
		// ht.getCOpts().click();
		// ht.getCountry().click();
		
		 ht.getHotels().click();
		 
		 ht.getOrigin().click();
		 
		 Thread.sleep(3000);
		
		 ht.getInputD().sendKeys(city);
		 
		System.out.println(ht.getInputD());
		 
		 Thread.sleep(3000);
		 
		 ht.getOpt().click();

		 ht.getDate().click();
		 
		 ht.getReturn().click();
		 
		 ht.getRoomGuests().click();
		 Thread.sleep(3000);
		 
		 ht.getAdults().click();
		 ht.getChildren().click();
		 
		 
		 
		 ht.getAge(picker);
	 
		 WebElement staticDropdown = driver.findElement(By.xpath("//*[@id=\'0\']"));
		 Select dropdown =new Select(staticDropdown);
			//dropdown.selectByIndex(3); //selects the fourth option of the index
			dropdown.selectByVisibleText("4");
		ht.applyFeatures().click();
		ht.getReason().click();
		
		ht.getLeisure().click();
		 
		ht.searchHotels().click();
		
		
		 
		List<WebElement> links = driver.findElements(By.xpath("//a[@rel='nofollow']"));
		
		
		
		System.out.println("The search throws "+ links.size() + " properties");
		
		for (WebElement link:links) {
			
			
			
			System.out.println(link.getAttribute("href"));
			
		
			
			
		}
		
	
		
		
		
	/*	Date d = new Date();
		 
		String FileName = d.toString().replace(":", "_").replace(" ", "_") + ".png";
 
		File screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
 
		FileHandler.copy(screenshot, new File("C:\\Users\\User\\Documents\\mmtrip\\screenshots\\" + "HotelsTest_"+ FileName));

	*/
 
	}

		
		

		
			
			
	 

		
		
				
			
	
	 
	

	@AfterTest
	public void closedriver(){
	 
		driver.close();
	}
	
	}

	


