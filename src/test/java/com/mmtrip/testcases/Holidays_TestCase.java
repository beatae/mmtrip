package com.mmtrip.testcases;

import com.mmtrip.pages.HolidayzPage;
import com.mmtrip.pages.MakeMyTripPage;
import com.mmtrip.resources.Base;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.io.IOException;


public class Holidays_TestCase extends Base {

    public WebDriver driver;
    public WebDriverWait w;


    @BeforeTest
    public void startDriver() throws IOException {
        driver = initializeDriver();
        w = initializeWait();
        driver.manage().window().maximize();
    }

    @Test
    public void baseNavigation() {

        driver.get("https://www.makemytrip.com"); //
        driver.getCurrentUrl();
        System.out.println(driver.getCurrentUrl());
        MakeMyTripPage mmt = new MakeMyTripPage(driver, w);
        mmt.evadePopup();
        mmt.scrollToSuperOffers();
        mmt.waitMore();
        mmt.getMore().click();
        mmt.getHolidays().click();
        WebElement slickList=mmt.getSlickList();
        mmt.selectOffer(slickList);
        mmt.switchToNewTab();
        HolidayzPage h=new HolidayzPage(driver,w);
        h.evadeSkipButton();
        h.evadeNextButton();
        h.evadeHelpButtons();
    }

    @AfterTest
    public void closedriver() {

        driver.close();
        driver.quit();

    }

}
