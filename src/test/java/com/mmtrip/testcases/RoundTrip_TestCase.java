package com.mmtrip.testcases;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.io.FileHandler;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.mmtrip.pages.MakeMyTripPage;
import com.mmtrip.pages.RoundTrip;
import com.mmtrip.resources.Base;
import com.mmtrip.resources.ScreenshotCapture;



public class RoundTrip_TestCase extends Base {
	
	public WebDriver driver;
	
	@BeforeTest
	public void startDriver() throws IOException{
		driver = initializeDriver();
		driver.manage().window().maximize();
		
	}
	
	@Test
	public void baseNavigation() throws IOException, InterruptedException {
				
		
		 driver.get("https://www.makemytrip.com/flights/?ccde=us"); 
		
		 String place = "Bangkok";
		 String date = "Wed Jun 23 2021";
		
			
		 MakeMyTripPage mmt=new MakeMyTripPage(driver);
		// ScreenshotCapture sc= new ScreenshotCapture(driver);
		 
		
		 mmt.evadePopup();
	
		 
		 RoundTrip rt = new RoundTrip(driver);
		// rt.getPop().click();
		 
		 rt.getRt().click();
		 	 
		 
		 rt.getOrigin().click();
		 
		 rt.getOpt().click();
		// Thread.sleep(3000);
		 
		 rt.getInputD().sendKeys(place);
		 rt.getDCity().click();
		 
		 Thread.sleep(3000);
		 
		 rt.getDate().click();
		 
		 WebElement Calendar=driver.findElement(By.cssSelector("div.DayPicker-Months"));
		 List<WebElement> days=Calendar.findElements(By.cssSelector("div.DayPicker-Day"));
		 
		
			for(WebElement day:days)
		 {
			 //System.out.println(day.getAttribute("class"));
			 if(day.getAttribute("aria-label").contains(date))
			 {
				 System.out.println(day.getAttribute("class"));
				 day.click();
				 break;
			 }
		 }
		
		
		 
		 
		 
		 
		 //rt.getReturn().click();
		 
		 rt.getTravClass().click();

		 rt.getAdults().click();
		 
		 rt.getChildren().click();
		 
		 rt.getTClass().click();
		 
		 rt.applyFeatures().click();
		 
		 rt.searchFlights().click();
		 
		 
		
		 	Date d = new Date();
		 
			String FileName = d.toString().replace(":", "_").replace(" ", "_") + ".png";
	 
			File screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
	 
			FileHandler.copy(screenshot, new File("C:\\Users\\User\\Documents\\mmtrip\\screenshots\\" + "RoundTrip_"+ FileName));
	
			
	
		 
	}
	
	
	@AfterTest
	public void closedriver(){
	 
		driver.close();
	 
	}
	
}

