package com.mmtrip.testcases;

import java.io.IOException;
import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.mmtrip.pages.MakeMyTripPage;
import com.mmtrip.resources.Base;

public class MultyCity_TestCase extends Base {

	private WebDriver driver;
	private WebDriverWait w;


	@BeforeTest
	public void startDriver() throws IOException {
		driver = initializeDriver();
		w = initializeWait();
		driver.manage().window().maximize();
	}

	@Test
	public void baseNavigation() {

		driver.get("https://www.makemytrip.com"); //
		driver.getCurrentUrl();

		System.out.println(driver.getCurrentUrl());
		MakeMyTripPage mmt = new MakeMyTripPage(driver, w);

		mmt.evadePopup();
		mmt.getMultiCity().click();
		mmt.getAddAnotherCity().click();
		List<WebElement> fromCities= mmt.getFromCities();
		System.out.println(fromCities.size());
		List<WebElement> toCities= mmt.getToCities();
		System.out.println(toCities.size());
		selectMultyCity(fromCities,mmt);
	}

	@AfterTest
	public void closedriver() {
		driver.close();
	}

	private void selectCity(String destiny,MakeMyTripPage mmt, String searchFrom,String cityFrom,int index) {
		if(destiny.equals("from"))
		{mmt.getFrom().sendKeys(searchFrom);}
		else {mmt.getTo().sendKeys(searchFrom);}

		List<WebElement> suggestionsFrom = mmt.getSuggestions();
		mmt.validateOptions(suggestionsFrom,cityFrom);
		mmt.printIataSuggestions(suggestionsFrom);
		mmt.selectAirport(suggestionsFrom, suggestionsFrom.get(index).getText());
	}

	private void selectMultyCity(List<WebElement> fromCities, MakeMyTripPage mmt) {
		int usa;
		int india;
		for(int i=0;i<3;i++)
		{
			if(i==0)
			{
				fromCities.get(i).click();
				selectCity("from",mmt, ",United States", "Cities United States",i);
				selectCity("to",mmt, ",India", "Cities India",i+1);
			}else
			{
				usa=6*(i+1)+1;
				india=i+5;
				fromCities.get(i).click();
				selectCity("from",mmt, ",United States", "Cities United States",usa);
				selectCity("to",mmt, ",India", "Cities India",india);
			}
			mmt.getToday().click();
		}

	}

}

