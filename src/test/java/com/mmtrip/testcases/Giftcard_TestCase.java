package com.mmtrip.testcases;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.io.FileHandler;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.mmtrip.pages.GirftcardPage;
import com.mmtrip.pages.MakeMyTripPage;
import com.mmtrip.resources.Base;



public class Giftcard_TestCase extends Base {
	
	public WebDriver driver;
	String amount = "5000";
	String sender = "John Doe";
	String sMobile = "1126597135";
	String sEmail = "user@gmail.com";
	
	@BeforeTest
	public void startDriver() throws IOException{
		driver = initializeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
	}
	
	@Test
	public void baseNavigation() throws IOException, InterruptedException {
				
		
		// driver.get("https://www.makemytrip.com/"); 
		 
		 MakeMyTripPage mmt=new MakeMyTripPage(driver);
		 
		 GirftcardPage mo = new GirftcardPage(driver);
		 
		 mmt.evadePopup();
		
		 		 
		
		 
		 	//Hovers on the other menu
		 	WebElement ele1 = driver.findElement(By.xpath("//ul/li[@data-cy='menu_More']"));
		    Actions action = new Actions(driver);
		    action.moveToElement(ele1).build().perform();

		  mo.getGift().click();
		 
		   mo.getACard().click();
		
		   mo.getTheme().click();
		   mo.getACard().click();
		   mo.getAmount().sendKeys(Keys.CONTROL,"a");
		   mo.getAmount().sendKeys(Keys.DELETE);
		   mo.getAmount().sendKeys(amount);
		   
		   mo.getSender().sendKeys(sender);
		   mo.getMobile().sendKeys(sMobile);
		   mo.getEmail().sendKeys(sEmail);
		   mo.setBuy().click();
		   
		   //Prints the payment options
		   System.out.println(mo.getPayOpts());
		   
		   
		   
		   //Sets the screenshot with a date
		   Date d = new Date();
			 
			String FileName = d.toString().replace(":", "_").replace(" ", "_") + ".png";
	 
			File screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
	 
			FileHandler.copy(screenshot, new File("C:\\Users\\User\\Documents\\mmtrip\\screenshots\\" + "GiftcardTest_"+ FileName));
		 
		 
		 
}

	@AfterTest
	public void closedriver(){
	 
		driver.close();
	}

	
	
	
	
}

