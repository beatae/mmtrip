package com.mmtrip.testcases;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.mmtrip.pages.CabsBookingPage;
import com.mmtrip.pages.CabsPage;
import com.mmtrip.pages.MakeMyTripPage;
import com.mmtrip.resources.ExcelData;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.mmtrip.resources.Base;

public class Cabs_TestCase extends Base {

    public WebDriver driver;
    public WebDriverWait w;


    @BeforeTest
    public void startDriver() throws IOException {
        driver = initializeDriver();
        w = initializeWait();
        driver.manage().window().maximize();
    }

    @Test(dataProvider="getData")
    public void baseNavigation(String cityFrom,String cityTo,String airportFrom,String airportTo) {

        driver.get("https://www.makemytrip.com"); //
        driver.getCurrentUrl();
        System.out.println(driver.getCurrentUrl());
        MakeMyTripPage mmt = new MakeMyTripPage(driver, w);
        mmt.evadePopup();
        validateCountry(mmt);
        mmt.getCabsButton().click();
        CabsBookingPage cp=new CabsBookingPage(driver,w);
        cp.getFromCity().click();
        cp.getFrom().sendKeys(cityFrom);
        List<WebElement> suggestionsFrom=cp.getSuggestions();
        System.out.println(suggestionsFrom.size());
        cp.selectAirport(suggestionsFrom,airportFrom);
        cp.getTo().sendKeys(cityTo);
        List<WebElement> suggestionsTo = cp.getSuggestions();
        cp.selectAirport(suggestionsTo, airportTo);
        System.out.println(driver.findElement(By.xpath("//div[@role='heading']//div")).getText());
        cp.getDate().click();
        validateReturn(cp);
        cp.getHour().click();
        cp.getSearch().click();
        CabsPage cpb=new CabsPage(driver,w);
        cpb.getCabs();
    }

    @AfterTest
    public void closedriver() {
        driver.close();
    }

    @DataProvider
    public Object[][] getData() throws IOException {
        Object[][] data=new Object[1][4];
        ExcelData d=new ExcelData();
        ArrayList<String> excelData=d.getExcelData("Cabs");

        for (int i = 0; i < 4; i++) {
            data[0][i]=excelData.get(i+1);
        }
        return data;
    }

    private void validateCountry(MakeMyTripPage mmt) {
        if(mmt.getCountrySelect().getText().equals("IN"))
        {
            System.out.println("Es el pais correcto");
        }else
        {
            System.out.println("No es el pais correcto");
        }

    }

    private boolean returnIsDisabled(CabsBookingPage mmt) {
        return mmt.getReturnDate().getText().equals("Tap to add a return date for bigger discounts");
    }

    private void validateReturn(CabsBookingPage trip) {
        if (returnIsDisabled(trip)) {
            System.out.println("Return option is disabled");
        } else {
            System.out.println("Return option is enabled");
        }

    }

}

