package com.mmtrip.testcases;

import java.io.File;
import java.io.IOException;
import java.util.Date;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.io.FileHandler;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.mmtrip.pages.MakeMyTripPage;
import com.mmtrip.pages.TrainsPage;
import com.mmtrip.resources.Base;



public class Trains_TestCase extends Base {
	
	public WebDriver driver;
	
	@BeforeTest
	public void startDriver() throws IOException{
		driver = initializeDriver();
		driver.manage().window().maximize();
		
	}
	
	@Test
	public void baseNavigation() throws IOException, InterruptedException {
				
		
		
		 TrainsPage ts = new TrainsPage(driver);
		
		 MakeMyTripPage mmt=new MakeMyTripPage(driver);
		 
		 
		 
		 mmt.evadePopup();
		// ts.getCOpts().click();
		 
		// ts.getCountry().click();
		 
		 ts.getTrains().click();
		 
		 ts.getLiveStat().click();
		 
		 ts.getTrain().click();
		
		 ts.getOrigin().sendKeys("12724");
		 
		 ts.setOrigin().click();
		 Thread.sleep(3000);
			
		 ts.setTStop().click();
		 
		 ts.setDate().click();
		 
		 ts.checkStatus().click();
		 Thread.sleep(3000);
		 
		 
		 System.out.println(ts.getTrainHour());
		 
		 //tests screenshots
			Date d = new Date();
			String FileName = d.toString().replace(":", "_").replace(" ", "_") + ".png";
	 		File screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
	 		FileHandler.copy(screenshot, new File("C:\\Users\\User\\Documents\\mmtrip\\screenshots\\" + "TrainsTest_"+ FileName));
	 		
		
}
	
	@AfterTest
	public void closedriver(){
	 
		driver.close();
	 
	}
	
}

