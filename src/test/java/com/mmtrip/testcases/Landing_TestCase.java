package com.mmtrip.testcases;

import java.io.File;
import java.io.IOException;
import java.util.Date;



import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.io.FileHandler;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.reporter.ExtentSparkReporter;
import com.mmtrip.pages.LandingPage;
import com.mmtrip.pages.MakeMyTripPage;
import com.mmtrip.resources.Base;



public class Landing_TestCase extends Base {
	ExtentReports extent;
	private WebDriverWait w;
	
	public WebDriver driver;
	
	@BeforeTest
	public void startDriver() throws IOException{
		driver = initializeDriver();
		driver.manage().window().maximize();
		w = initializeWait(); 
		
	}
	
	@Test
	public void baseNavigation() throws IOException, InterruptedException {
				
		
	
		
		 
		 LandingPage lp = new LandingPage(driver);
		 
	
		 MakeMyTripPage mmt=new MakeMyTripPage(driver);
		 
		 
		 mmt.evadePopup();
		 
		 lp.getCountry().click();

		 lp.getCountryName().click();// click on usa
		 
		 lp.getUsa().click();
		 
		 lp.applyCountry().click();
				 
		Thread.sleep(3000);
		
		// gets title of the page
		 String newcurrent = driver.getTitle();
		 System.out.println(newcurrent);
		 String title = "MakeMyTrip USA - #1 Travel Website for Flight Booking, Airline Tickets";

		 		 
		
		 try {
			    Assert.assertEquals(newcurrent, title);
			} catch (AssertionError e) {
			    System.out.println("Incorrect");
			    throw e;
			}
			System.out.println("Correct");
			
			
			
			
			
			Date d = new Date();
			 
			String FileName = d.toString().replace(":", "_").replace(" ", "_") + ".png";
	 
			File screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
	 
			FileHandler.copy(screenshot, new File("C:\\Users\\User\\Documents\\mmtrip\\screenshots\\" + "LandingTest_"+ FileName));
			 
			
			
			
			
	}
	@AfterTest
	public void closedriver(){
	 
		driver.close();
	 
	}
	
}

