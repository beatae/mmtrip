package com.mmtrip.testcases;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.mmtrip.pages.HotelsPage;
import com.mmtrip.pages.MakeMyTripPage;
import com.mmtrip.resources.ExcelData;
import org.openqa.selenium.WebDriver;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.mmtrip.resources.Base;

public class HotelBook_TestCase extends Base {

    public WebDriver driver;
    public WebDriverWait w;


    @BeforeTest
    public void startDriver() throws IOException {
        driver = initializeDriver();
        w = initializeWait();
        driver.manage().window().maximize();
    }

    @Test(dataProvider="getData")
    public void baseNavigation(String hotelCity,String type,String childAge1,String childAge2,String reason) {

        driver.get("https://www.makemytrip.com"); //
        driver.getCurrentUrl();
        System.out.println(driver.getCurrentUrl());
        MakeMyTripPage mmt = new MakeMyTripPage(driver, w);
        mmt.evadePopup();
        validateCountry(mmt);
        mmt.getHotelsButton().click();
        HotelsPage hpage=new HotelsPage(driver,w);
        hpage.getSelectHtlCity().click();
        hpage.waitEnterHotel();
        hpage.getEnterHotel().sendKeys(hotelCity);
        List<WebElement> hotelSuggestions=hpage.getSuggestions();
        System.out.println(hotelSuggestions.size());
        hpage.selectHotel(hotelSuggestions,type);
        hpage.getCheckIn().click();
        hpage.getCheckOut().click();
        hpage.getRoomGuest().click();
        hpage.getAdultsHB().click();
        hpage.getChildrenHB().click();
        hpage.selectChildAge(1,childAge1);
        hpage.selectChildAge(2,childAge2);
        hpage.getApply().click();
        hpage.getTravelForReasonTxt().click();
        hpage.selectReason(reason);
        hpage.getSearchBtn().click();

    }

    @AfterTest
    public void closedriver() {

        driver.close();

    }

    @DataProvider
    public Object[][] getData() throws IOException {
        Object[][] data=new Object[1][5];
        ExcelData d=new ExcelData();
        ArrayList<String> excelData=d.getExcelData("HotelBook");
        //review Generics
        for (int i = 0; i < 5; i++) {
            data[0][i]=excelData.get(i+1);
        }
        return data;
    }

    private void validateCountry(MakeMyTripPage mmt)
    {
        if(mmt.getCountrySelect().getText().equals("IN"))
        {
            System.out.println("Es el pais correcto");
        }else
        {
            System.out.println("No es el pais correcto");
        }

    }

}

